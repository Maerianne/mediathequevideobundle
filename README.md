MaesboxVideoBundle
====================
[![Latest Stable Version](https://poser.pugx.org/maesbox/videobundle/v/stable.svg)](https://packagist.org/packages/maesbox/videobundle) [![Total Downloads](https://poser.pugx.org/maesbox/videobundle/downloads.svg)](https://packagist.org/packages/maesbox/videobundle) [![Latest Unstable Version](https://poser.pugx.org/maesbox/videobundle/v/unstable.svg)](https://packagist.org/packages/maesbox/videobundle) [![License](https://poser.pugx.org/maesbox/videobundle/license.svg)](https://packagist.org/packages/maesbox/videobundle)

Présentation 
--------------------

Ce bundle permet la gestion des fichiers vidéos sous forme de médiathèque.

Installation
--------------------

- installation avec composer

~~~~
"require": {
    ...
    "maesbox/videobundle": "dev-master",
    ...
},
~~~~


Configuration
--------------------

- dans le fichier "app/routing.yml" :

~~~~

~~~~

- dans le fichier "app/parameters.yml" :

~~~~

~~~~